[![pipeline status](https://git.coop/aptivate/ansible-roles/ssh-user-mgmt/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/ssh-user-mgmt/commits/master)

# ssh-user-mgmt

An role to manage SSH based user accounts.

# Requirements

## SSH Key and Username

  * An SSH Key (if generating a new one, please read [this]).
  * A desired username.

[this]: https://blog.g3rt.nl/upgrade-your-ssh-keys.html

You'll need to follow convention to add yourself to the repository:

  * Add a username the `usernames` list in `defaults/main.yml`.
  * Add a SSH public key part called `<username>.pub` to `files`.

The role logic will pick that up and proceed to create accounts.

# Role Variables

  * `enable_passwordless_sudo`: allow passwordless user sudo access.
    * Default is `true`.

  * `ssh_user_names`: the user accounts to create
    * Defaults to current Aptivate staff.

  * `ssh_user_groups`: the user groups to create
    * Defaults to the base groups Aptivate staff need.

# Dependencies

None.

# Example Playbook

```yaml
- hosts: my-linode-hosts
  roles:
     - role: ssh-user-mgmt
```

# Testing

We use Linode for our testing here.

You'll need to expose the `LINODE_API_KEY` environment variable for that.

```
$ pipenv install --dev
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
