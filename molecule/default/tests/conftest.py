import os

import pytest
from testinfra.utils.ansible_runner import AnsibleRunner

DEFAULT_HOST = 'all'

inventory = os.environ['MOLECULE_INVENTORY_FILE']
runner = AnsibleRunner(inventory)
runner.get_hosts(DEFAULT_HOST)
