import pytest


def test_ssh_installed(host):
    ssh_package = host.package('openssh')

    assert ssh_package.is_installed


def test_ssh_is_running(host):
    ssh_service = host.service('sshd')

    assert ssh_service.is_running
    assert ssh_service.is_enabled


@pytest.mark.parametrize(
    'user', (
        'lukem',
    ),
)
def test_home_and_ssh_directories_created(user, host):
    home_dir = '/home/{}'.format(user)
    assert host.file(home_dir).exists

    ssh_folder = '/home/{}/.ssh'.format(user)
    assert host.file(ssh_folder).exists

    authorized_keys = '/home/{}/.ssh/authorized_keys'.format(user)
    assert host.file(authorized_keys).exists


@pytest.mark.parametrize(
    'user', (
        'lukem',
    ),
)
def test_authorized_key_contents_in_place(user, host):
    authorized_keys = host.file('/home/{}/.ssh/authorized_keys'.format(user))
    user_key = open('../../files/{}.pub'.format(user), 'r').read()
    assert authorized_keys.contains(user_key)


@pytest.mark.parametrize(
    'user', (
        'lukem',
    ),
)
def test_id_rsa_pub_contents_in_place(user, host):
    id_rsa = host.file('/home/{}/.ssh/id_rsa.pub'.format(user))
    user_key = open('../../files/{}.pub'.format(user), 'r').read()
    assert id_rsa.contains(user_key)
