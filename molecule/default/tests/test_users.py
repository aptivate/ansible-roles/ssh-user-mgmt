import pytest


@pytest.mark.parametrize(
    'user', (
        'lukem',
    ),
)
def test_user_created(user, host):
    assert host.user(user).exists


def test_user_wheel_passwordless_access(host):
    ssh_config = host.file('/etc/sudoers')
    with host.sudo():
        assert ssh_config.contains('%wheel ALL=(ALL) NOPASSWD: ALL')
